'use strict';

const URL = 'https://ajax.test-danit.com/api/swapi/films';

const container = document.createElement('div');
container.classList.add('container');
document.body.append(container);

const episode = document.createElement('div');
episode.classList.add('episode__about');
container.append(episode);


fetch(URL)
.then(data =>data.json())
.then(films => {
    sortFilms(films);
})



function sortFilms(array){

    for(let obj of array){

        let episodeName = document.createElement('h2');
        episodeName.style.textTransform = 'uppercase';

        episodeName.textContent = obj.id +  " | " + obj.name;
        episodeName.classList.add('text');

        episode.append(episodeName);

        let openingCrawl = document.createElement('p');
        openingCrawl.classList.add('text');
        openingCrawl.textContent = obj.openingCrawl;
        episode.append(openingCrawl);

       

        
        let ul = document.createElement('ul');

  // person fetch
      let characterPromises = obj.characters.map(characterUrl => {
        return fetch(characterUrl)
          .then(response => response.json())
          .then(personData => {
            let personName = personData.name;
            let character = document.createElement('li');
            character.classList.add('text');

            character.textContent = personName;
            ul.append(character);


          });

          
      });

    

        episode.append(ul);


        
        Promise.all(characterPromises)
        .then(()=>{
            episode.style.animationName = 'scroll';
        })

    }
      
}
